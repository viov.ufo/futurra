const state = () => ({
    abtest: null,
    promoStatus: true, 
    promoTime: 180,
})
  
const mutations = {
    setAbtest(state, value) {
      state.abtest = value
    },
    setPromoTime(state, value) {
      state.promoTime = value
      if(value === 0) state.promoStatus = false
    },
}
const getters = {
    getDesignType: state => state.abtest,
    getPromoTime: state => state.promoTime,
    getPromoStatus: state => state.promoStatus
}
export default {
    namespased: true,
    state,
    getters,
    mutations,
}